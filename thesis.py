# Students:     Stamatis Grispos & Miltiadis Vouzounaras
# Supervisors:  Alexandros Alexandridis & Gregory Koulouras
# Institute:    Technological Educational Institute of Athens
# Faculty:      Technological Applications
# Department:   Electronic Engineering
# MSc Program:  Design and Development of Advanced Electronic Systems

import numpy as np
import pandas as pd
import statistics
import math
import time
import matplotlib.pyplot as plt
from collections import Counter


def arr_2d_to_3d(arr_2d, arr_3d):
    """
    This function outputs a 3-dimensional array, indexed by the first column
    of the input 2-dimensional array.
    """
    lab = arr_2d[0, 0]              # Starting index
    times = arr_2d.shape[0]         # Calculates the length of the y axis
    column = arr_2d.shape[1]        # Calculates the length of the
    y_2d, y_3d, z_3d = 0, 0, 0      # Initialise variables

    for y_2d in range(times):
        if lab != arr_2d[y_2d, 0]:  # Changes z axis when index is different
            y_3d = 0
            z_3d += 1
            lab = arr_2d[y_2d, 0]
        arr_3d[z_3d, y_3d, 0:column - 1] = arr_2d[y_2d, 1:column]  # Copy the row from the 2d to the 3d
        y_3d += 1
    return arr_3d


def most_frequent_from_list(input_list):
    """
    This function returns the most frequent item of the input list.
    """
    occurrence_count = Counter(input_list)
    return occurrence_count.most_common(1)[0][0]


def unique_from_list(input_list):
    """
    This function returns the unique items of the input list.
    """
    x = np.array(input_list)
    return np.unique(x)


def expand_lessons(input_array, start_time, duration):
    """
    This function returns a version of the input array
    expanded by the duration of each lesson.
    """
    hours_counter = 0

    for row in range(input_array.shape[0]):
        hours_counter += (input_array[row, duration]) / 100
    output_array = np.zeros((int(hours_counter), input_array.shape[1]), dtype=int)

    row_out = 0

    for row in range(input_array.shape[0]):
        output_array[row_out, :] = input_array[row, :]
        for hours in range(int(input_array[row, duration] / 100)):
            output_array[row_out + hours, :] = input_array[row, :]
            output_array[row_out + hours, start_time] = input_array[row, start_time] + (100 * hours)
        row_out += int(input_array[row, duration] / 100)
    return output_array


def add_time_code(input_array, day_id_column, time_id_column):
    """
    This function add a column at the end of the input
    array with a time-coded value for each row.
    exp: day_id = 5, time_id = 1200, time_code = (5 * 100) + (1200 / 100) = 512
    """
    output_array = np.zeros((input_array.shape[0], input_array.shape[1] + 1), dtype=int)
    for row in range(input_array.shape[0]):
        output_array[row, 0:input_array.shape[1]] = input_array[row, :]
        output_array[row, -1] = (input_array[row, day_id_column] * 100) + int(input_array[row, time_id_column] / 100)
    return output_array


def fitness_std(lesson_chromosome, erg_id):
    """
    This function calculates the fitness_std???
    More information to be added.
    """
    # Matlab function [Fit_std,sum1]=Std_Fitness(Chrom,Erg_id)
    l_stats = lessons_stats_struct(lesson_chromosome, erg_id)
    sum_std = 0
    for i_erg in range(int(erg_id.shape[0])):  # var i renamed i_erg to solve a warning
        if l_stats[i_erg, 11] != 0:
            sum_std += (l_stats[i_erg, 10] / l_stats[i_erg, 11])
    fit_std = sum_std / int(erg_id.shape[0])
    return fit_std


def conflicts(AM, chrome_with_time):
    """
    This function calculates the conflicts of the input student
    and returns the data in an array.
    """
    # Matlab function [Conf,Kena]=Conflicts(Foit_Struct,AM)
    conf = np.zeros((1, 4))
    timestamps = chrome_with_time[np.where(chrome_with_time[:, 0] == AM), 11]
    # kmath=np.array([122,123,122,128,131,215,213,214,218,219,314,315,311,312,512,513,514,516,517])
    gaps = np.zeros((1, 5), dtype=int)
    days = np.unique([math.trunc(value) for value in timestamps[0][:] / 100])  # Find which days the student have lesson
    num_of_days = len(days)  # Find how many days student have lesson
    for i in range(len(gaps[0])):
        lesson_of_the_day = timestamps[(timestamps >= ((i + 1) * 100)) & (timestamps < ((i + 2) * 100))]
        # Finds Hours of the day that student have a lesson
        unique_hours_of_day = len(np.unique(lesson_of_the_day))  # Find how many different hours the student coming
        if unique_hours_of_day != 0:
            gaps[0, i] = 1 + max(lesson_of_the_day) - min(lesson_of_the_day) - unique_hours_of_day
            # find the gaps per day of the student

    stud_thes = np.where(chrome_with_time[:, 0] == AM)[0]
    thes_erg = np.where(chrome_with_time[stud_thes, 2] == 1) + stud_thes[0]
    thes_theor = np.where(chrome_with_time[stud_thes, 2] == 0) + stud_thes[0]

    erg_timestamps = chrome_with_time[thes_erg, 11]
    theor_timestamps = chrome_with_time[thes_theor, 11]
    conf[0][3] = num_of_days
    conf[0][0] = erg_timestamps.shape[1] - np.unique(erg_timestamps).shape[0]
    # Conflicts Lab with Lab
    conf[0][1] = theor_timestamps.shape[1] - np.unique(theor_timestamps).shape[0]
    # Conflicts Theory with Theory
    conf[0][2] = np.where(np.in1d(theor_timestamps, erg_timestamps))[0].shape[0]
    # timestamps.shape[1] - np.unique(timestamps).shape[0] - conf[0][0] - conf[0][1]
    # Conflicts lab with theory
    return gaps, conf


def diaf_dil(lesson_chromosome_start, lesson_chromosome, Students_ID, unique_labs_id, data_foit_p_array, data_less_p_array):

    """
    This functions calculates the differences between the initial students statement
    and the new suggested statement.
    """
    # Matlab function [sun_diaf,diaf_Tupikou,diaf_Mi_Tupikou,Dil_Typ,Dil_Mi_Typ,Diaf_T_Ana_Foititi]
    # =Diaf_Dil(Chrom,Chrom_a,Foit,Less,Student_id)%Arxiki dilosi,Dilosi algorithmo,Stoixeia Foititon
    # lesson_chromosome_start = np.array(lesson_chromosome)  # Just for testing. MUST removed

    dil_typ = 0
    dil_mi_typ = 0

    diaf_ana_foit = np.zeros((len(Students_ID), 4), dtype=int)
    # Student_ID , Diaf_Sunoliki, Diaf_Typ_Eks,  Diaf_Mi_Typ_Eks
    diaf_ana_foit[:, 0] = Students_ID

    diaf_ana_math = np.zeros((len(unique_labs_id), 4), dtype=int)
    # Unique_labs_id , Diaf_Sunoliki, Diaf_Typ_Eks,  Diaf_Mi_Typ_Eks
    diaf_ana_math[:, 0] = unique_labs_id

    sum_diaf = np.zeros((1, 3), dtype=int)
    # Diaf_Sunoliki, Diaf_Typ_Eks,  Diaf_Mi_Typ_Eks

    # diaf_typ_ana_foit=np.zeros((len(Students_ID),2),dtype=int)
    # diaf_typ_ana_foit[:,0]=Students_ID
    # diaf_mi_typ_ana_foit=diaf_typ_ana_foit

    # diaf_typ_ana_math=np.zeros((len(unique_labs_id),2),dtype=int)
    # diaf_typ_ana_math[:,0]=unique_labs_id
    # diaf_mi_typ_ana_math=diaf_typ_ana_math

    for i in range(unique_labs_id.shape[0]):
        # c[i,0]=unique_labs_id[i]
        lesson_semester = data_less_p_array[data_less_p_array[:, 0] == unique_labs_id[i], 4][0]

        diaf_ana_math[i, 1] = np.count_nonzero(lesson_chromosome_start[i, :, 1] != lesson_chromosome[i, :, 1])
        # measure the differences per lesson
        for j in range(lesson_chromosome_start[i, np.nonzero(lesson_chromosome_start[i, :, 0]), 0].shape[1]):

            # print(lesson_chromosome_start[i, np.nonzero(lesson_chromosome_start[i, :, 1]), 0].shape[1])
            student_semester = data_foit_p_array[data_foit_p_array[:, 0] == lesson_chromosome_start[i, j, 0], 1][0]
            # find student semester
            if lesson_semester == student_semester:
                dil_typ += 1
                if lesson_chromosome_start[i, j, 1] != lesson_chromosome[i, j, 1]:
                    diaf_ana_foit[np.where(diaf_ana_foit[:, 0] == lesson_chromosome[i, j, 0])[0][0], 2] += 1
                    diaf_ana_math[i, 2] += 1
            else:
                dil_mi_typ += 1
                if lesson_chromosome_start[i, j, 1] != lesson_chromosome[i, j, 1]:
                    diaf_ana_foit[np.where(diaf_ana_foit[:, 0] == lesson_chromosome[i, j, 0])[0][0], 3] += 1
                    diaf_ana_math[i, 3] += 1

    sum_diaf[0][0] = sum(diaf_ana_math[:, 1])
    sum_diaf[0][1] = sum(diaf_ana_math[:, 2])
    sum_diaf[0][2] = sum(diaf_ana_math[:, 3])
    diaf_ana_foit[:, 1] = diaf_ana_foit[:, 2] + diaf_ana_foit[:, 3]

    return sum_diaf, diaf_ana_foit, dil_typ, dil_mi_typ


def lessons_stats_struct(lesson_chromosome, erg_id):

    """
    This function calculates the nesseseary statistics about the chromosome.
    """
    # Matlab function L_stats=Lesson_Stats_Struct(Chrom,Erg_id)
    l_stats = np.zeros((len(erg_id), 14), dtype=float)
    for i in range(int(erg_id.shape[0])):
        l_stats[i, 8] = np.count_nonzero(lesson_chromosome[i, :, 0])
        # Number of students in this lesson
        for j in range(int(erg_id[i, 1] + 2)):
            l_stats[int(i), int(j)] = int(len(np.where(lesson_chromosome[int(i), 0:int(l_stats[i, 8]), 1] == (int(j) - 1))[0]))
            # 1= No reg, 2= Without attendance, 3-8 Classrooms
        l_stats[i, 9] = (l_stats[i, 8] - l_stats[i, 0] - l_stats[i, 1]) / erg_id[i, 1]
        # Mean value of Students/classroom
        l_stats[i, 10] = np.std(l_stats[i][2:(erg_id[i, 1] + 2):1])
        # Standard Deviation of students per classrooms
        var = np.zeros(erg_id[i, 1])
        # Create array to calculate Max Standard Deviation of students per classrooms
        var[0] = sum(l_stats[i][2:8])
        # Create array to calculate Max Standard Deviation of students per classrooms
        l_stats[i, 11] = np.std(var)
        # Max Standard Deviation of students per classrooms
        l_stats[i, 12] = erg_id[i, 0]
        # Lesson ID
        l_stats[i, 13] = erg_id[i, 1]
        # Lesson number of classrooms

    return l_stats


def most_common(input_list):
    return statistics.mode(input_list)


def conflicts_2hours(students_chromosome_3d, number_of_students, students_id, closest_index):

    """
    This function returns the amount of conflicts per lesson of the student.
    """
    # Matlab function Conf_str=Conflicts_dioron(DNA_foititi)
    # foit_struct_conf = np.zeros([number_of_students, students_chromosome_3d.shape[1]]) - 1  # ?????
    # for i in range(students_id.shape[0]):

    num_less = np.where(students_chromosome_3d[closest_index, :, 0] > 0)[0].shape[0]
    # Num_Less=size(Split_DNA,2);%Πόσα Lesson DNA έχω.
    k_ores = np.zeros([num_less, 6])
    num_dna = np.zeros([num_less, 10])
    for j in range(num_less):
        num_dna = students_chromosome_3d[closest_index, :, :]
        hours = num_dna[j, 5] / 100
        for k in range(int(hours)):
            k_ores[j, k + 1] = 100 * num_dna[j, 3] + num_dna[j, 4] / 100 + k
    k_ores[:, 0] = num_dna[0:num_less, 0]

    for j in range(num_less):
        for k in range(4):
            if k_ores[j, k + 1] != 0:
                k_ores[j, 5] = k_ores[j, 5] + np.where(k_ores[j, k + 1] == k_ores[:, 1:4])[0].shape[0] - 1

    return k_ores


def conflicts_2hours2(students_chromosome_3d, number_of_students, students_id):
    """
    This function returns the amount of conflicts per lesson of all students.
    """
    # foit_struct_conf = np.zeros([number_of_students, students_chromosome_3d.shape[1]]) - 1  # ?????
    k_ores = np.zeros([number_of_students, students_chromosome_3d.shape[1], 6])
    # 1D Students, 0-LessonID 1-4 Timestamps 5-Conflicts of Lesson

    for i in range(students_id.shape[0]):
        num_less = np.where(students_chromosome_3d[i, :, 0] > 0)[0].shape[0]
        # Num_Less=size(Split_DNA,2);%Πόσα Lesson DNA έχω.
        num_dna = np.zeros([num_less, 10])
        for j in range(num_less):
            num_dna = students_chromosome_3d[i, :, :]
            hours = num_dna[j, 5] / 100
            for k in range(int(hours)):
                k_ores[i, j, k + 1] = 100 * num_dna[j, 3] + num_dna[j, 4] / 100 + k
        k_ores[i, :, 0] = num_dna[:, 0]

        for j in range(num_less):
            for k in range(4):
                if k_ores[i, j, k + 1] != 0:
                    k_ores[i, j, 5] = k_ores[i, j, 5] + np.where(k_ores[i, j, k + 1] == k_ores[i, :, 1:4])[0].shape[0] - 1
                    # Conflicts of the lesson with other lessons of Student

    return k_ores


def conflicts_2hours_mask(students_chromosome_3d, students_id, k_ores):

    """
    This function returns the conflicts only for the lab lessons.
    """
    # Matlab function [DNA_Foit_Array,DNA_Conf_Array]=Conflicts_dioron_MASK(DNA_foititi,DNA_foititi_Conf,dataMath)
    # foit_struct_conf=np.zeros([number_of_students,students_chromosome_3d.shape[1]])-1

    DNA_Conf_array = np.array(k_ores[:, :, 5])

    for i in range(students_id.shape[0]):
        # Sinartisi
        theseis = np.where(DNA_Conf_array[i, :] != 0)
        if np.where(DNA_Conf_array[i, :] != 0)[0].shape[0] > 0:
            for j in range(theseis[0].shape[0]):
                DNA_Conf_array[i, theseis[0][j]] = DNA_Conf_array[i, theseis[0][j]] * students_chromosome_3d[i, theseis[0][j], 1]
                # Conflicts of LABS
        # Sinartisi

    return DNA_Conf_array


def conflicts_2hours_mask_stud(students_chromosome_3d, students_id, closest_index, DNA_Conf_array):
    """
    This function returns the conflicts only for the lab lessons for one student.
    """
    # Matlab function [DNA_Foit_Array,DNA_Conf_Array]=Conflicts_dioron_MASK(DNA_foititi,DNA_foititi_Conf,dataMath)
    # foit_struct_conf=np.zeros([number_of_students,students_chromosome_3d.shape[1]])-1

    # for i in range(students_id.shape[0]):
    # Sinartisi
    theseis = np.where(DNA_Conf_array[closest_index, :] != 0)
    if theseis[0].shape[0] > 0:
        for j in range(theseis[0].shape[0]):
            DNA_Conf_array[closest_index, theseis[0][j]] = DNA_Conf_array[closest_index, theseis[0][j]] * students_chromosome_3d[closest_index, theseis[0][j], 1]
            # Conflicts of LABS
    # Sinartisi

    return DNA_Conf_array


def diaf_dil_rand(lesson_chromosome, lesson_chromosome_start, erg_id, students_id, unique_labs_id):

    """
    Maybe not used (?)
    """
    # Matlab function [sun_diaf,diaf_Tupikou,diaf_Mi_Tupikou,Dil_Typ,Dil_Mi_Typ,Diaf_T_Ana_Foititi]
    # =Diaf_Dil(Chrom,Chrom_a,Foit,Less,Student_id)%Arxiki dilosi,Dilosi algorithmo,Stoixeia Foititon
    # lesson_chromosome_start = np.array(lesson_chromosome)  # Just for testing. MUST removed
    # RAND
    lesson_chromosome_rand = np.array(lesson_chromosome)
    for i in range(lesson_chromosome_rand.shape[0]):
        for j in range(np.where(lesson_chromosome_rand[i, :, 0] > 0)[0].shape[0]):
            if lesson_chromosome_rand[i, j, 1] > 0:
                lesson_chromosome_rand[i, j, 1] = np.random.randint(1, erg_id[i, 1] + 1)
    # RAND

    # Dataframe imports must be transferred to the main program
    data_foit_p_array = pd.DataFrame(data_foit_p, columns=['spec_aem', 'exam_id']).to_numpy()
    data_less_p_array = pd.DataFrame(data_less_p, columns=['Module_id',
                                                           'Number_of_classes',
                                                           'Dynamic_Classroom',
                                                           'Lab',
                                                           'semester']).to_numpy()

    dil_typ = 0
    dil_mi_typ = 0

    diaf_ana_foit = np.zeros((len(students_id), 4), dtype=int)
    # Student_ID , Diaf_Sunoliki, Diaf_Typ_Eks,  Diaf_Mi_Typ_Eks
    diaf_ana_foit[:, 0] = students_id

    diaf_ana_math = np.zeros((len(unique_labs_id), 4), dtype=int)
    # Unique_labs_id , Diaf_Sunoliki, Diaf_Typ_Eks,  Diaf_Mi_Typ_Eks
    diaf_ana_math[:, 0] = unique_labs_id

    sum_diaf = np.zeros((1, 3), dtype=int)  # Diaf_Sunoliki, Diaf_Typ_Eks,  Diaf_Mi_Typ_Eks

    # diaf_typ_ana_foit=np.zeros((len(Students_ID),2),dtype=int)
    # diaf_typ_ana_foit[:,0]=Students_ID
    # diaf_mi_typ_ana_foit=diaf_typ_ana_foit

    # diaf_typ_ana_math=np.zeros((len(unique_labs_id),2),dtype=int)
    # diaf_typ_ana_math[:,0]=unique_labs_id
    # diaf_mi_typ_ana_math=diaf_typ_ana_math

    for i in range(unique_labs_id.shape[0]):
        lesson_semester = data_less_p_array[data_less_p_array[:, 0] == unique_labs_id[i], 4][0]
        diaf_ana_math[i, 1] = np.count_nonzero(lesson_chromosome_start[i, :, 1] != lesson_chromosome_rand[i, :, 1])
        # measure the differences per lesson
        for j in range(lesson_chromosome_start[i, np.nonzero(lesson_chromosome_start[i, :, 1]), 0].shape[1]):
            student_semester = data_foit_p_array[data_foit_p_array[:, 0] == lesson_chromosome_start[i, j, 0], 1][0]
            # find student semester
            if lesson_semester == student_semester:
                dil_typ += 1
                if lesson_chromosome_start[i, j, 1] != lesson_chromosome_rand[i, j, 1]:
                    diaf_ana_foit[np.where(diaf_ana_foit[:, 0] == lesson_chromosome_rand[i, j, 0])[0][0], 2] += 1
                    diaf_ana_math[i, 2] += 1
            else:
                dil_mi_typ += 1
                if lesson_chromosome_start[i, j, 1] != lesson_chromosome_rand[i, j, 1]:
                    diaf_ana_foit[np.where(diaf_ana_foit[:, 0] == lesson_chromosome_rand[i, j, 0])[0][0], 3] += 1
                    diaf_ana_math[i, 3] += 1

    sum_diaf[0][0] = sum(diaf_ana_math[:, 1])
    sum_diaf[0][1] = sum(diaf_ana_math[:, 2])
    sum_diaf[0][2] = sum(diaf_ana_math[:, 3])
    diaf_ana_foit[:, 1] = diaf_ana_foit[:, 2] + diaf_ana_foit[:, 3]

    return sum_diaf, diaf_ana_foit


def lesson_chrom_to_stud_chrom(lesson_chromosomes_array, lesson_chromosome, number_of_students, students_chromosome_3d,
                               Students_ID, erg_id):

    """
    This function creates an array with the data per student.
    """
    students_chromosome_3d_RAND = np.array(students_chromosome_3d)
    for i in range(lesson_chromosome.shape[0]):
        for j in range(np.where(lesson_chromosome[i, :, 0] > 0)[0].shape[0]):
            # stud=np.where(Students_ID==lesson_chromosome[i,0,0])[0][0]
            stud_thes = np.where(Students_ID == lesson_chromosome[i, j, 0])[0][0]
            less_thes = np.where(students_chromosome_3d[stud_thes, :, 0] == erg_id[i, 0])
            # erg_thes=np.where(students_chromosome_3d[stud_thes,:,1]==1)
            new_lesson_pop = lesson_chromosomes_array[np.where(lesson_chromosomes_array[:, 0] == erg_id[i, 0])[0][lesson_chromosome[i, j, 1] - 1], :]
            # Foit_pop[closest_index, thes_erg2[0][closest_index_Less], :] = new_lesson_pop

            students_chromosome_3d_RAND[stud_thes, less_thes, :] = new_lesson_pop

    return students_chromosome_3d_RAND


def changes_var(Init_Change, Fin_Change, Tot_Gens, Gens_Num, DNA_Conf_array, diaf_ana_foit, Students_ID,
                students_chromosome_3d, lesson_chromosome, lesson_chromosomes_array, erg_id, number_of_students,
                k_ores):

    """
    This function make the changes on the chromosome.
    """
    # Matlab function [new_pop,Foit_pop,Foit_pop_MASK,Changes_AM]=Changes_var4(init_change, fin_change, totgen,gensn,
    # Chrom_arx,Chrom_ch,Foit_Struct_Conf_MASK,Foit_Struct,Erg_id,Student_id,dataMath,
    # Pinakas,rawdataAnath,dataFoit,dataLess)
    Changes_AM = np.array([])
    change_counter = 0
    # Auta tha mpainoun os eisodoi
    # Init_Change = 5
    # Fin_Change = 5
    # Tot_Gens = 500000
    # Gens_Num = 50
    # Auta tha mpainoun os eisodoi
    Gens_Num = gens_p
    Tot_Gens = gens
    Max_Change = Init_Change + Fin_Change - (Init_Change * (Gens_Num - 1) + Fin_Change * (Tot_Gens - Gens_Num)) / (Tot_Gens - 1)
    # Maximum number of changes
    Syn_Changes = np.random.randint(round(Max_Change)) + 1

    # MATLAB isos prepei na to metatrepso
    Students_ID2 = np.array(Students_ID)  # Student_id2=Student_id;%
    Foit_pop = np.array(students_chromosome_3d)
    Foit_pop_MASK = np.array(DNA_Conf_array)
    Chrom2 = np.array(lesson_chromosome)
    # Foit_pop=Foit_Struct;
    # Foit_pop_MASK=Foit_Struct_Conf_MASK;
    while change_counter < Syn_Changes:
        change_counter += 1

        # diaf_ana_foit από Diaf_Dil_p αρχ με αλλαγμένο
        stud_no_change_typ = diaf_ana_foit[np.where(diaf_ana_foit[:, 2] == 0), 0]
        # stud_no_change_typ=diaf_ana_foit[np.where(diaf_ana_foit[:,2]==0),0]
        # Foit_xoris_Diafora=find(Roul_Diaf_T(:,2)==0)
        stud_with_change_typ = diaf_ana_foit[np.where(diaf_ana_foit[:, 2] != 0), 0]
        sum_students_no_change_typ = stud_no_change_typ.shape[1]
        sum_students_with_change_typ = stud_with_change_typ.shape[1]

        if sum_students_with_change_typ > 0:
            pos_ana_diaf_typ = 0.8 / sum(diaf_ana_foit[:, 2])  # sum_students_with_change_typ
            pos_ana_mi_diaf_typ = 0.2 / sum_students_no_change_typ
            # pos_ana_mi_diaf_typ=0.2/(sum_students_no_change_typ-np.where(Students_LABS[:,1]==0)[0].shape[0])
        else:
            pos_ana_diaf_typ = 0
            pos_ana_mi_diaf_typ = 1 / sum_students_no_change_typ
            # pos_ana_mi_diaf_typ=1/(sum_students_no_change_typ-np.where(Students_LABS[:,1]==0)[0].shape[0])
            # Pos_ana_Mi_diaf_T=1/size(Foit_xoris_Diafora,1)
        Roul_Diaf_T = np.zeros([number_of_students, 4])
        Roul_Diaf_T[:, 0] = Students_ID

        Roul_Diaf_T = Roul_Diaf_T.astype(float)  # ALLAGI SE FLOAT PINAKA
        thes_me_diaf_typ = np.where(diaf_ana_foit[:, 2] == 0)
        # thes_xoris_diaf_typ=np.where(diaf_ana_foit[:,2]!=0)
        Roul_Diaf_T[np.where(diaf_ana_foit[:, 2] != 0), 2] = pos_ana_diaf_typ * sum(diaf_ana_foit[np.where(diaf_ana_foit[:, 2] != 0), 2])
        Roul_Diaf_T[thes_me_diaf_typ, 2] = pos_ana_mi_diaf_typ

        # diaf_ana_foit[np.where(diaf_ana_foit[:,2]==0),2]=pos_ana_mi_diaf_typ
        # diaf_ana_foit[np.where(diaf_ana_foit[:,2]!=0),2]=pos_ana_diaf_typ
        # *sum(diaf_ana_foit[np.where(diaf_ana_foit[:,2]!=0),1])
        Roul_Diaf_T[0, 3] = Roul_Diaf_T[0, 2]

        for i in range(Roul_Diaf_T.shape[0] - 1):
            Roul_Diaf_T[i + 1, 3] = Roul_Diaf_T[i, 3] + Roul_Diaf_T[i + 1, 2]

        random_value = np.random.rand()
        # min_value = min(abs(Roul_Diaf_T[:, 3] - random_value))
        index_of_min_value = np.where(abs(Roul_Diaf_T[:, 3] - random_value) == min(abs(Roul_Diaf_T[:, 3] - random_value)))[0][0]
        # [c index] = min(abs(Roul_Diaf_T(:,4)-R_timi));%Επιλογή ενός φοιτητή τυχαία..
        if Roul_Diaf_T[index_of_min_value, 3] > random_value:
            closest_stud = diaf_ana_foit[index_of_min_value - 1, 0]
            closest_index = index_of_min_value - 1
        else:
            closest_stud = diaf_ana_foit[index_of_min_value, 0]
            closest_index = index_of_min_value

        ### SINARISTI CONFLICTS MASK
        Foit_Conf = DNA_Conf_array[closest_index, :]
        ### SINARISTI CONFLICTS
        Foit_Less = students_chromosome_3d[closest_index, :, :]

        thes_erg2 = np.where(Foit_Less[:, 1] == 1)

        if thes_erg2[0].shape[0] > 0:
            foit_conf_roul = np.zeros([thes_erg2[0].shape[0], 4])
            foit_conf_roul[:, 0] = Foit_Less[thes_erg2, 0]
            foit_conf_roul[:, 1] = Foit_Conf[thes_erg2] + 0.5
            foit_conf_roul[:, 2] = foit_conf_roul[:, 1] / sum(foit_conf_roul[:, 1])

            foit_conf_roul[0, 3] = foit_conf_roul[0, 2]
            for i in range(foit_conf_roul.shape[0] - 1):
                foit_conf_roul[i + 1, 3] = foit_conf_roul[i, 3] + foit_conf_roul[i + 1, 2]

            random_value = np.random.rand()
            index_of_min_value = np.where(abs(foit_conf_roul[:, 3] - random_value) == min(abs(foit_conf_roul[:, 3] - random_value)))[0][0]
            if foit_conf_roul[index_of_min_value, 3] > random_value:
                closest_less = foit_conf_roul[index_of_min_value, 0]
                closest_index_Less = index_of_min_value
            else:
                closest_less = foit_conf_roul[index_of_min_value + 1, 0]
                closest_index_Less = index_of_min_value + 1

            num_of_classes = erg_id[np.where(erg_id[:, 0] == closest_less), 1][0][0]
            rand_choice = np.random.randint(1, num_of_classes + 1)
            new_lesson_pop = lesson_chromosomes_array[np.where(lesson_chromosomes_array[:, 0] == closest_less)[0][rand_choice - 1], :]
            Foit_pop[closest_index, thes_erg2[0][closest_index_Less], :] = new_lesson_pop
            less_position = np.where(erg_id[:, 0] == closest_less)[0][0]
            Chrom2[less_position, np.where(Chrom2[less_position, :, 0] == closest_stud), 1] = rand_choice
            # lesson_chromosome[np.where(erg_id[:,0==closest_Less),
            Students_ID2 = np.delete(Students_ID2, np.where(Students_ID2 == closest_stud), 0)
            Changes_AM = np.append(Changes_AM, [closest_stud], axis=0)

            # OTAN FTIAKSEI MILOTS SINARTISEIS NA PROSARMOSO KAI AUTA
            k_ores_stud = conflicts_2hours(Foit_pop, number_of_students, Students_ID, closest_index)
            k_ores_zeros = np.zeros([k_ores.shape[1], k_ores.shape[2]])
            k_ores_zeros[np.where(k_ores_stud[:, 0] > 0), :] = k_ores_stud
            k_ores[closest_index, :, :] = k_ores_zeros
            # k_ores = conflicts_2hours2(Foit_pop, number_of_students, Students_ID)
            # Foit_pop_MASK = conflicts_2hours_mask(Foit_pop, Students_ID, k_ores)
            Foit_pop_MASK = conflicts_2hours_mask_stud(Foit_pop, Students_ID, closest_index, Foit_pop_MASK)

            # Conf_D=Conflicts_dioron(DNA_foit);%Νέα conflicts διώρων του φοιτητή.
            # [~,Conf_D_MASK]=Conflicts_dioron_MASK(DNA_foit,Conf_D,dataMath);%Νέα conflicts διώρων του φοιτητή με μάσκα.

    return Chrom2, Foit_pop, Foit_pop_MASK, Changes_AM


def rand_lesson_chrom(lesson_chromosome, erg_id):

    """
    This function makes an random initial chromosome.
    """
    # Matlab function [Chrom_Rand,Pinakas_ch,Foit_Struct_ch,Foit_Struct_Conf_ch,
    # Foit_Struct_Conf_MASK_ch,AM_Conf_ch,Kena_ch,AM_Kena_ch]
    # =Rand_Chrom(Pinakas,rawdataAnath,dataMath)
    lesson_chromosome_rand = np.array(lesson_chromosome)
    for i in range(lesson_chromosome_rand.shape[0]):

        for j in range(np.where(lesson_chromosome_rand[i, :, 0] > 0)[0].shape[0]):
            if lesson_chromosome_rand[i, j, 1] > 0:
                lesson_chromosome_rand[i, j, 1] = np.random.randint(1, erg_id[i, 1] + 1)

    return lesson_chromosome_rand


def student_with_labs(Students_ID, students_chromosome_3d):
    students_labs = np.zeros([Students_ID.shape[0], 2])
    students_labs[:, 0] = Students_ID
    for i in range(students_chromosome_3d.shape[0]):
        students_labs[i, 1] = sum(students_chromosome_3d[i, :, 1])

    return students_labs


def syn_conf(Students_ID, chrome_with_time):
    Conf_all = np.zeros((1, 4))
    Kena_all = np.zeros((1, 5))

    Conf = np.zeros((1, 4))
    Kena = np.zeros((1, 5))
    for s in range(Students_ID.shape[0]):

        AM = Students_ID[s]
        # ANTIKATASTASI APO SUNARTISI >> Conf=Conflicts(chrome_with_time)
        timestamps = chrome_with_time[np.where(chrome_with_time[:, 0] == AM), 11]
        # kmath=np.array([122,123,122,128,131,215,213,214,218,219,314,315,311,312,512,513,514,516,517])
        gaps = np.zeros((1, 5), dtype=int)
        days = np.unique([math.trunc(value) for value in timestamps[0][:] / 100])
        # Find which days the student have lesson
        num_of_days = len(days)  # Find how many days student have lesson
        for i in range(len(gaps[0])):
            lesson_of_the_day = timestamps[(timestamps >= ((i + 1) * 100)) & (timestamps < ((i + 2) * 100))]
            # Finds Hours of the day that student have a lesson
            unique_hours_of_day = len(np.unique(lesson_of_the_day))  # Find how many different hours the student coming
            if unique_hours_of_day != 0:
                gaps[0, (i)] = 1 + max(lesson_of_the_day) - min(lesson_of_the_day) - unique_hours_of_day
                # find the gaps per day of the student

        # students_chromosome_3d[5, :, :] forgotten?
        stud_thes = np.where(chrome_with_time[:, 0] == AM)[0]
        thes_erg = np.where(chrome_with_time[stud_thes, 2] == 1) + stud_thes[0]
        thes_theor = np.where(chrome_with_time[stud_thes, 2] == 0) + stud_thes[0]

        erg_timestamps = chrome_with_time[thes_erg, 11]
        theor_timestamps = chrome_with_time[thes_theor, 11]

        Conf[0][0] = erg_timestamps.shape[1] - np.unique(erg_timestamps).shape[0]
        # Conflicts Lab with Lab
        Conf[0][1] = theor_timestamps.shape[1] - np.unique(theor_timestamps).shape[0]
        # Conflicts Theory with Theory
        Conf[0][2] = np.where(np.in1d(theor_timestamps, erg_timestamps))[0].shape[0]
        # timestamps.shape[1] - np.unique(timestamps).shape[0] - Conf[0][0] - Conf[0][1]
        # Conflicts lab with theory
        Conf[0][3] = num_of_days
        # ////ANTIKATASTASI APO SUNARTISI

        Conf_all = np.append(Conf_all, Conf, axis=0)
        Kena = np.append(Kena, gaps, axis=0)
    Conf_all = np.delete(Conf_all, 0, 0)
    Kena = np.delete(Kena, 0, 0)
    Conf_sum = Conf_all.sum(axis=0)
    Kena_sum = Kena.sum(axis=0)

    return Conf_all, Kena, Conf_sum, Kena_sum


def meg_conf_foit(Students_ID, students_chromosome_3d, chrome_with_time):
    Meg_Conf_Foit = np.zeros((len(Students_ID), 1))
    i = 0
    while i < len(Students_ID):
        Meg_Conf_Foit[i] = sum(students_chromosome_3d[i, :, 5]) / 100  # Maybe need to change to integer
        i += 1

    # [Conf,AM_Conf,Kena,AM_Kena]=Syn_Conf(chrome_with_time,Students_ID)
    # Βρίσκουμε Conflicts συνολικά και ανάλογα με αριθμό μητρώου, και τα συνολικα κενά-με αριθμό μητρώου.
    # Meg_Kena_Foit=AM_Conf(:,5)*12-Meg_Conf_Foit(:);%Μέγιστα κενά ανά φοιτητή
    # Meg_Conf_Syn=sum(Meg_Conf_Foit);%μέγιστος αριθμος Conflicts
    # Meg_Kena_Syn=sum(Meg_Kena_Foit);%μέγιστος αριθμος κενών

    [Conf_all, Kena, Conf_sum, Kena_sum] = syn_conf(Students_ID, chrome_with_time)
    # Βρίσκουμε Conflicts συνολικά και ανάλογα με αριθμό μητρώου, και τα συνολικα κενά-με αριθμό μητρώου.
    Meg_Kena_Foit = Conf_all[:, 3] * 12 - Meg_Conf_Foit[:, 0]  # Μέγιστα κενά ανά φοιτητή
    Meg_Conf_Syn = sum(Meg_Conf_Foit)  # μέγιστος αριθμος Conflicts
    Meg_Kena_Syn = sum(Meg_Kena_Foit)  # μέγιστος αριθμος κενών

    return Meg_Conf_Syn, Meg_Kena_Syn


def fit_conf(Conf_sum, Kena_sum, Meg_Conf_Syn, Meg_Kena_Syn, number_of_students):
    Fit_Conf = sum(Conf_sum[0:3]) / Meg_Conf_Syn
    Fit_Kena = sum(Kena_sum) / Meg_Kena_Syn
    Fit_Meres = (Conf_sum[3] / number_of_students) / 5

    # Eisodoi: Conf_sum, Kena_sum,Meg_Conf_syn, Meg_Kena_syn, number_of_students
    # eksodoi: Fit_Conf Fit_Kena Fit_Meres

    return Fit_Conf, Fit_Kena, Fit_Meres


def fit_diaf_dil(lesson_chromosome_start, lesson_chromosome, Students_ID, unique_labs_id):
    [sum_diaf, diaf_ana_foit, dil_typ, dil_mi_typ] = diaf_dil(lesson_chromosome_start, lesson_chromosome,
                                                              Students_ID, unique_labs_id, data_foit_p_array,
                                                              data_less_p_array)

    Fit_Diaf_T = sum_diaf[0][1] / dil_typ
    Fit_Diaf_Mi_T = sum_diaf[0][2] / dil_mi_typ
    Fit_Diaf_Syn = sum_diaf[0][0] / (dil_typ + dil_mi_typ)

    return Fit_Diaf_T, Fit_Diaf_Mi_T, Fit_Diaf_Syn, diaf_ana_foit


def fitness_tel(Conf_sum, Kena_sum, Meg_Conf_Syn, Meg_Kena_Syn, number_of_students, lesson_chromosome_start,
                lesson_chromosome, Students_ID, unique_labs_id, erg_id, weights):
    [Fit_Conf, Fit_Kena, Fit_Meres] = fit_conf(Conf_sum, Kena_sum, Meg_Conf_Syn, Meg_Kena_Syn, number_of_students)
    [Fit_Diaf_T, Fit_Diaf_Mi_T, Fit_Diaf_Syn, diaf_ana_foit] = fit_diaf_dil(lesson_chromosome_start, lesson_chromosome,
                                                                            Students_ID, unique_labs_id)
    Fit_std = fitness_std(lesson_chromosome, erg_id)
    w_fit_diaf_t = weights[0]
    w_fit_diaf_mi_t = weights[1]
    w_fit_diaf_syn = weights[2]
    w_fit_conf = weights[3]
    w_fit_kena = weights[4]
    w_fit_meres = weights[5]
    w_fit_std = weights[6]

    fit_main = np.array([(w_fit_diaf_t * Fit_Diaf_T + w_fit_diaf_mi_t * Fit_Diaf_Mi_T +
                          w_fit_diaf_syn * Fit_Diaf_Syn + w_fit_conf * Fit_Conf +
                          w_fit_kena * Fit_Kena + w_fit_meres * Fit_Meres + w_fit_std * Fit_std) /
                         (w_fit_diaf_t + w_fit_diaf_mi_t + w_fit_diaf_syn +
                          w_fit_conf + w_fit_kena + w_fit_meres + w_fit_std),
                         Fit_Diaf_T, Fit_Diaf_Mi_T, Fit_Diaf_Syn, Fit_Conf, Fit_Kena, Fit_Meres, Fit_std])

    return fit_main, diaf_ana_foit


def update_conf_array(Changes_AM, Foit_pop, Conf_all, Kena, Students_ID):

    """
    This function updates the arrays to skip calculating the conflicts for the whole array.
    """
    for i in range(Changes_AM.shape[0]):
        var = np.append((np.zeros([Foit_pop.shape[1]]) + Changes_AM[i]).reshape(17, 1),
                        Foit_pop[np.where(Changes_AM[i] == Students_ID)[0][0]], axis=1)
        # var[np.where(var[:,1]==0),0]=0

        [gaps, conf] = conflicts(Changes_AM[i], add_time_code(expand_lessons(var[np.where(var[:, 1] > 0)[0], :], 5, 6), 4, 5))
        thes = np.where(Students_ID == Changes_AM[i])
        Conf_all[thes, :] = conf
        Kena[thes, :] = gaps

    return Conf_all, Kena


def make_lesson_chromosome(pinakas_p):
    # Make lesson chromosome old
    # labs_values = pinakas_p[pinakas_p.Lab == 1]         # Κρατάει μόνο τις γραμμές που είναι το Lab = 1
    # unique_labs_id = labs_values['Module_id'].unique()  # Βρίσκει μόνο τα ID των εργαστηρίων
    # number_of_labs = len(unique_labs_id)                # Βρίσκει τον αριθμό των εργαστηρίων
    # labs_values_lessons = labs_values['Module_id'].tolist()
    # labs_values_lessons_unique = np.unique(labs_values_lessons)
    # labs_values_students = labs_values['spec_aem'].tolist()
    # labs_values_selection = labs_values['Selection_by_Student'].tolist()
    # id_most_common = statistics.mode(labs_values_lessons)   # Finds the most common lesson
    # id_most_common_number = labs_values_lessons.count(id_most_common)   # Counts the how many times appears
    # zero_lesson_chromosome = np.zeros((number_of_labs, id_most_common_number, 2), dtype=int)
    # Creates the zero chromosome
    # labs_values_2d = labs_values.loc[:, ['Module_id', 'spec_aem', 'Selection_by_Student']].to_numpy()
    # lesson_chromosome = arr_2d_to_3d(labs_values_2d, zero_lesson_chromosome)

    # Make lesson chromosome new

    labs_values = pinakas_p[pinakas_p.Lab == 1]  # Κρατάει μόνο τις στήλες που είναι το Lab 1
    unique_labs_id = labs_values['Module_id'].unique()  # Βρίσκει μόνο τα ID των εργαστηρίων
    number_of_labs = len(unique_labs_id)  # Βρίσκει τον αριθμό των εργαστηρίων
    Labs_Values_Lessons = labs_values['Module_id'].tolist()
    Labs_Values_Students = labs_values['spec_aem'].tolist()
    Labs_Values_Selection = labs_values['Selection_by_Student'].tolist()

    id_most_common = most_common(Labs_Values_Lessons)
    id_most_common_number = Labs_Values_Lessons.count(id_most_common)
    # Bellow added from old lesson chromosome code
    zero_lesson_chromosome = np.zeros((number_of_labs, id_most_common_number, 2), dtype=int)
    # Creates the zero chromosome
    labs_values_2d = labs_values.loc[:, ['Module_id', 'spec_aem', 'Selection_by_Student']].to_numpy()
    lesson_chromosome = arr_2d_to_3d(labs_values_2d, zero_lesson_chromosome)

    # i=1
    # while i < Number_Of_Labs:
    #     i +=1

    # Θέλω στο Data frame Labs_Values, να βρίσκουμε ποιο vmodule_id υπάρχει τις περισσότερες φορές,
    # και πόσες είναι αυτές. Ας πούμε ID_Most_Common και ID_Most_Common_Number.
    # Στην συνέχεια φτιάξουμε ένα 3D Πίνακα με μηδενικά. Με διαστάσεις (Number_Of_Labs,ID_Most_Common_Number,2)
    # Και γεμίζουμε τον πίνακα με του αριθμούς μητρωόυ και τις δηλώσεις που αντιστοιχούν σε κάθε μάθημα.
    erg_id = np.zeros((len(unique_labs_id), 2), dtype=int)
    erg_id[:, 0] = unique_labs_id
    # Δεν παίρνει σωστά το Εαρινό. το βγάζει όλο σαν NUM
    data_math_p_array = pd.DataFrame(data_math_p, columns=['module_id', 'Number_of_classes', 'Εαρινό', 'IsLab',
                                                           'Δηλώσεις Φοιτητών', 'Δηλώσεις First Reg',
                                                           'Δηλώσεις Not First Reg', 'Άτομα ανα LAB (All)',
                                                           'Άτομα ανα LAB (Only First Reg)']).to_numpy()

    for i in range(unique_labs_id.shape[0]):
        erg_id[i, 1] = data_math_p_array[np.where(data_math_p_array[:, 0] == erg_id[i, 0]), 1]
        # Lesson number of classrooms

    return lesson_chromosome, erg_id, unique_labs_id


def make_students_chromosome(data_anath_p, pinakas_p):
    # Φτιάχνω το χρωμόσωμα κάθε μαθήματος.
    # Αυτά θα χρησιμοποιηθούν στην συνέχεια για την σύνθεση του χρωμοσώματος του κάθε φοιτητή
    lesson_chromosomes = pd.DataFrame(data_anath_p, columns=['Lesson_id', 'Lab', 'semester', 'Day_id', 'Start_Time',
                                                             'Duration', 'Classroom_id', 'Prof1_id', 'Prof2_id',
                                                             'Prof3_id'])

    lesson_chromosomes_array = lesson_chromosomes.to_numpy()

    # Κάνω τον πίνακα λίστα
    pinakas_p_df = pd.DataFrame(pinakas_p, columns=['Module_id', 'spec_aem', 'Selection_by_Student', 'Lab'])

    pinakas_p_array = pinakas_p_df.to_numpy()
    # Δημιουργία κενού πίνακα για να δημιουργηθεί το χρωμόσωμα των φοιτητών
    students_chromosome1 = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])

    i = 0
    while i < len(pinakas_p_array):
        # Εάν είναι εργαστήριο και έχει κάνει δήλωση ο φοιτητής
        if pinakas_p_array[i, 3] == 1 and pinakas_p_array[i, 2] > 0:
            index = np.where(lesson_chromosomes_array[:, 0] == pinakas_p_array[i, 0])
            # Πρέπει να βρω πως θα γυρίσω τον πίνακα
            students_chromosome1 = np.append(students_chromosome1, np.array([np.append([pinakas_p_array[i, 1]], lesson_chromosomes_array[index[0][(pinakas_p_array[i, 2] - 1)], ])]), axis=0)
            # Εάν είναι θεωρεία και δεν είναι πτυχιακή ή Πρακτική
        elif pinakas_p_array[i, 3] == 0 and pinakas_p_array[i, 0] < 79:
            index = np.where(lesson_chromosomes_array[:, 0] == pinakas_p_array[i, 0])
            students_chromosome1 = np.append(students_chromosome1, np.array([np.append([pinakas_p_array[i, 1]], lesson_chromosomes_array[index[0][0], :])]), axis=0)
            if len(index[0][:]) == 2:
                students_chromosome1 = np.append(students_chromosome1, np.array([np.append([pinakas_p_array[i, 1]], lesson_chromosomes_array[index[0][1], :])]), axis=0)
        i += 1

    students_chromosome1 = np.delete(students_chromosome1, 0, 0)

    column_index = 0

    # Sort 2D numpy array by 2nd Column
    sorted_arr = students_chromosome1[students_chromosome1[:, column_index].argsort()]

    # print(sortedArr)
    students_chromosome = sorted_arr

    # List = [2, 1, 2, 2, 1, 3]
    # print(most_frequent_from_list(List))

    id_st_most_common = most_frequent_from_list(students_chromosome[:, 0].tolist())
    # Finds the student with the most lessons
    id_st_most_common_number = students_chromosome[:, 0].tolist().count(id_st_most_common)
    # Counts the how many times appears

    Students_ID = unique_from_list(students_chromosome1[:, 0])
    number_of_students = len(Students_ID)

    # Creates the zero chromosome
    zero_students_chromosome = np.zeros((number_of_students, id_st_most_common_number, 10), dtype=int)
    students_chromosome_3d = arr_2d_to_3d(students_chromosome, zero_students_chromosome)

    chrome = expand_lessons(students_chromosome, 5, 6)
    chrome_with_time = add_time_code(chrome, 4, 5)
    return students_chromosome, students_chromosome_3d, chrome_with_time, Students_ID, number_of_students, lesson_chromosomes_array


# Start of the main program

# Read the necessary spreadsheets for the make lesson chromosome


spoydon = 'Programma_spoydon_vol33.xlsx'

pinakas_p = pd.read_excel(spoydon, sheet_name='Constant_DNA')
data_math_p = pd.read_excel(spoydon, sheet_name='Μαθήματα')
data_foit_p = pd.read_excel(spoydon, sheet_name='Φοιτητές')
data_less_p = pd.read_excel(spoydon, sheet_name='Constant_DNA')
data_anath_p = pd.read_excel(spoydon, sheet_name='Αναθέσεις (2014-2015-Ε)')
data_foit_p_array = pd.DataFrame(data_foit_p, columns=['spec_aem', 'exam_id']).to_numpy()
data_less_p_array = pd.DataFrame(data_less_p, columns=['Module_id', 'Number_of_classes', 'Dynamic_Classroom',
                                                       'Lab', 'semester']).to_numpy()

# Second run! Good Luck :-P

[lesson_chromosome, erg_id, unique_labs_id] = make_lesson_chromosome(pinakas_p)
[students_chromosome, students_chromosome_3d, chrome_with_time, Students_ID, number_of_students,
 lesson_chromosomes_array] = make_students_chromosome(data_anath_p, pinakas_p)
[Meg_Conf_Syn, Meg_Kena_Syn] = meg_conf_foit(Students_ID, students_chromosome_3d, chrome_with_time)
[Conf_all, Kena, Conf_sum, Kena_sum] = syn_conf(Students_ID, chrome_with_time)  # Conf_sum to cross check with MatLab

# filesav=['w1=' num2str(w1) 'w2=' num2str(w2) 'w3=' num2str(w3) 'w4=' num2str(w4)
# 'w5=' num2str(w5) 'w6=' num2str(w6) 'w7=' num2str(w7) datestr(now,'yyyymmdd_HHMMSS') type '_An3'];
# filesav(1,find(filesav(1,:)=='.'))='_';
# Time2(1)=0;

temp_min = 1
temp_max = 100
temp_current = temp_max
k_boltzman = 5.0000e-04
gens = 500000
alpha = 10 ** (math.log10(1 / 100) / gens)

w_fit_diaf_t = 4
w_fit_diaf_mi_t = 0
w_fit_diaf_syn = 0
w_fit_conf = 3
w_fit_kena = 1
w_fit_meres = 0
w_fit_std = 2

Init_Change = 5
Fin_Change = 5

weights = (w_fit_diaf_t, w_fit_diaf_mi_t, w_fit_diaf_syn, w_fit_conf, w_fit_kena, w_fit_meres, w_fit_std)

fit_array = np.zeros([gens + 2, 8])
# 0-Fit_Syn 1-Fit_diaf_t 2-Ftit_diaf_mi_T 3-Fit_Diaf_Syn 4-Fit_Conf 5-Fit_Kena 6-Fit_Meres 7-Fit_std

# # #RAND
# lesson_chromosome_rand = np.array(lesson_chromosome)
# for i in range(lesson_chromosome_rand.shape[0]):
#     for j in range(np.where(lesson_chromosome_rand[i, :, 0] > 0)[0].shape[0]):
#         if lesson_chromosome_rand[i, j, 1] > 0:
#             lesson_chromosome_rand[i, j, 1] = np.random.randint(1, erg_id[i, 1]+1)

# pop_ch = np.array(lesson_chromosome_rand)  # lesson chromosome
# students_chromosome_3d_Rand=lesson_chrom_to_stud_chrom(lesson_chromosomes_array,pop_ch,number_of_students,students_chromosome_3d,Students_ID,erg_id)
# students_chromosome_3d=np.array(students_chromosome_3d_Rand)
# # #RAND

# INITIAL values
pop_ch = np.array(lesson_chromosome)  # lesson chromosome
# INITIAL values

pop_arx = np.array(lesson_chromosome)  # lesson chromosome
Foit_pop = np.array(students_chromosome_3d)
k_ores = conflicts_2hours2(students_chromosome_3d, number_of_students, Students_ID)
DNA_Conf_array = conflicts_2hours_mask(students_chromosome_3d, Students_ID, k_ores)
[sum_diaf, diaf_ana_foit_arx, dil_typ, dil_mi_typ] = diaf_dil(pop_arx, pop_ch, Students_ID, unique_labs_id, data_foit_p_array, data_less_p_array)
l_stats = lessons_stats_struct(pop_ch, erg_id)
Foit_pop_MASK = np.array(DNA_Conf_array)
[fit_array[0, :], diaf_ana_foit] = fitness_tel(Conf_sum, Kena_sum, Meg_Conf_Syn, Meg_Kena_Syn, number_of_students,
                                               pop_arx, pop_ch, Students_ID, unique_labs_id, erg_id, weights)
# [old_fit,Fit_Diaf_T(1),Fit_Diaf_Mi_T(1),Fit_Diaf_Syn(1),Fit_Conf(1),Fit_Kena(1),Fit_Meres(1),Fit_std(1)]
# = Fitness_TEL(AM_Conf,AM_Kena,Chrom_Arx,pop_ch,dataFoit,dataLess,Erg_id,
# Meg_Kena_Syn,Meg_Conf_Syn,Student_id,w1,w2,w3,w4,w5,w6,w7);

iold_fit = fit_array[0, 0]

bestopopever = np.array(pop_ch)
best_Foit_popever = np.array(Foit_pop)
fitbestpopever = np.array(iold_fit)

# fitcurr(1)=1/iold_fit; #to exo balei sto fitness tel!!!!

gens_p = 0
# Stats2 = np.zeros([gens + 2, 6])

# Changes to review again
AM_Conf = Conf_all
AM_Kena = Kena
start_time = 0

while gens_p < gens:  # while temp_current > temp_min:
    gens_p += 1
    # fitcurr mallon katargeitai i fit_array[gens_p,0]=1/iold_fit
    # [sum_diaf, diaf_ana_foit, dil_typ, dil_mi_typ] = diaf_dil(pop_arx, pop_ch, Students_ID, unique_labs_id, data_foit_p_array, data_less_p_array)
    [new_pop, new_Foit_pop, new_Foit_pop_MASK, Changes_AM] = changes_var(Init_Change, Fin_Change, gens, gens_p,
                                                                         Foit_pop_MASK, diaf_ana_foit, Students_ID,
                                                                         Foit_pop, pop_ch, lesson_chromosomes_array,
                                                                         erg_id, number_of_students, k_ores)
    # changes_var(Init_Change, Fin_Change, gens, gens_p, DNA_Conf_array, diaf_ana_foit)
    # [new_pop,new_Foit_pop,new_Foit_pop_MASK,Changes_AM]=Changes_var4(init_change, fin_change, gens,gens_p-1,
    # Chrom_Arx,pop_ch,Foit_pop_MASK,Foit_pop,Erg_id,Student_id,dataMath,Pinakas,rawdataAnath,dataFoit,dataLess);
    # SINARTISI CHANGES VAR Changes var

    # students_chromosome_3d_ch=lesson_chrom_to_stud_chrom(lesson_chromosomes_array, pop_ch, number_of_students, students_chromosome_3d_ch,
    #                          Students_ID, erg_id)

    [new_AM_Conf, new_AM_Kena] = update_conf_array(Changes_AM, new_Foit_pop, AM_Conf, AM_Kena, Students_ID)
    # Sinartisi update Conf Array
    [fit_array[gens_p, :], diaf_ana_foit_new] = fitness_tel(new_AM_Conf.sum(axis=0), new_AM_Kena.sum(axis=0),
                                                            Meg_Conf_Syn, Meg_Kena_Syn, number_of_students, pop_arx,
                                                            new_pop, Students_ID, unique_labs_id, erg_id, weights)
    # print(diaf_ana_foit[:,1].sum(axis=0),'-',diaf_ana_foit[:,2].sum(axis=0),'-',diaf_ana_foit[:,3].sum(axis=0))
    # fit_array[gens_p, :] = fitness_tel(Conf_sum, Kena_sum, Meg_Conf_Syn, Meg_Kena_Syn, number_of_students, sum_diaf,
    # pop_arx, lesson_chromosome, Students_ID, unique_labs_id, l_stats, erg_id, weights)
    ifit = 1 / fit_array[gens_p, 0]
    # Stats2[gens_p, 1] = ifit
    # Stats2[gens_p, 2] = iold_fit
    # Stats2[gens_p, 3] = Changes_AM.shape[0]
    # print(math.exp(-(iold_fit - ifit) / (k_boltzman * temp_current)))
    if ifit > iold_fit:
        iold_fit = ifit
        pop_ch = np.array(new_pop)
        Foit_pop = np.array(new_Foit_pop)
        Foit_pop_MASK = np.array(new_Foit_pop_MASK)
        AM_Conf = np.array(new_AM_Conf)
        AM_Kena = np.array(new_AM_Kena)
        best_Foit_pop_MASK = np.array(new_Foit_pop_MASK)
        bestpopever = np.array(new_pop)
        best_Foit_popever = np.array(new_Foit_pop)
        fitbestpopever = ifit
        diaf_ana_foit = np.array(diaf_ana_foit_new)
        # Stats2[gens_p, 0] = 1
    elif np.random.rand() <= math.exp(-(iold_fit - ifit) / (k_boltzman * temp_current)):
        # Stats2[gens_p, 0] = 2
        # Stats2[gens_p, 4] = math.exp(-(iold_fit - ifit) / (k_boltzman * temp_current))
        # print(Stats2[gens_p,4])
        iold_fit = ifit
        pop_ch = np.array(new_pop)
        Foit_pop = np.array(new_Foit_pop)
        Foit_pop_MASK = np.array(new_Foit_pop_MASK)
        AM_Conf = np.array(new_AM_Conf)
        AM_Kena = np.array(new_AM_Kena)
        diaf_ana_foit = np.array(diaf_ana_foit_new)
    temp_current = alpha * temp_current
    if gens_p % 500 == 0:
        speed = 500 / (time.process_time() - start_time)
        eta_hours = ((gens - gens_p) / speed) / 3600
        percent = 100 * gens_p / gens
        print(f"Gen:\t", gens_p, f"\tFitness:\t{ifit:.4f}", f"\tGenes/sec:\t{speed:.3f}", f"\t{percent:.2f}", f"\tETA:\t{eta_hours:.2f}")
        start_time = time.process_time()

        # Traceback (most recent call last):
        # File "thesis_fix.py", line 905, in <module>
        # [fit_array[gens_p, :], diaf_ana_foit_new] = fitness_tel(new_AM_Conf.sum(axis=0), new_AM_Kena.sum(axis=0),
        # IndexError: index 500000 is out of bounds for axis 0 with size 500000

    # if gens_p % 5000 == 0:
    #     pd.DataFrame(fit_array).to_csv("fit_array.csv", header=False, index=False)

# Stats=[fitcurr' fit' Fit_Diaf_T' Fit_Diaf_Mi_T' Fit_Diaf_Syn' Fit_Conf' Fit_Kena' Fit_Meres' Fit_std' Stats2];
# [Conf_b,AM_Conf_b,Kena_b,AM_Kena_b]=Syn_Conf(best_Foit_popever,Student_id);
# [Fit_std,Std_b]=Std_Fitness(bestpopever,Erg_id);
# [sun_diaf_b,diaf_Tupikou_b,diaf_Mi_Tupikou_b,Dil_Typ_b,Dil_ Mi_Typ_b]
# =Diaf_Dil(Chrom_Arx,bestpopever,dataFoit,dataLess);
